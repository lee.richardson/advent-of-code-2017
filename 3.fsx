let surfaceLengthFromRadius radius = if radius = 1 then 1 else (radius - 1) * 8

let areaFromRadius radius =
    List.reduce
        (fun acc value -> acc + (surfaceLengthFromRadius value))
        [1 .. radius]

let sideLengthFromArea = float >> sqrt >> floor >> int

let radiusFromValue area =
    seq { 1 .. area }
    |> Seq.map (fun r -> (r, areaFromRadius r))
    |> Seq.skipWhile (fun (_, area2) -> area2 < area)
    |> Seq.head
    |> fst

let cardinalPointsOnRadius radius =
    let area = areaFromRadius radius
    let sideLength = sideLengthFromArea area

    let firstStep = (area - (sideLength / 2))

    [1..3]
    |> List.map (fun i -> firstStep - (sideLength / 2 * 2) * i)
    |> List.append [firstStep]

let closest a possible =
    possible
    |> List.minBy (fun p -> abs (a - p))

//

let doIt input =
    let radius = radiusFromValue input

    let closestEdge =
        cardinalPointsOnRadius radius
        |> closest input

    (max input closestEdge) - (min input closestEdge) + radius - 1

doIt 325489

1,1,2,4,5,10,11,23,25,26,54,57,59,122,133,142,147,304,330,351,362,747,806