#r "FParsecCS.dll"
#r "FParsec.dll"
open FParsec.CharParsers
open FParsec.Primitives

let charsToString = Array.ofList >> System.String >> string

type Content =
    | Group of Content list
    | Garbage of Content list
    | String of string
    | Cancelled of char

let cancelled: Parser<Content, unit> =
    pchar '!' >>. anyChar
    |>> Cancelled

let emptyGarbage: Parser<Content, unit> = stringReturn "<>" (Garbage [String ""])
let garbageChars =
    (many1 (noneOf ">!")
    |>> (charsToString >> String))

let garbage, garbageImpl = createParserForwardedToRef ()
let garbageContents: Parser<Content, unit> = emptyGarbage <|> cancelled <|> garbageChars

garbageImpl :=
    between (pchar '<') (pchar '>') ((many (garbageContents)))
    |>> Garbage


let emptyGroup = pstring "{}" >>% (Group [])
let group, groupImpl = createParserForwardedToRef ()
let groupContents = (emptyGroup <|> garbage <|> group)

groupImpl :=
    between (pchar '{') (pchar '}') (sepBy groupContents (pchar ','))
    |>> Group

run group "{{}}"
run group "{{<>}}"
run group "{{{<>}}}"
run group "{{{<{<}>}}}"
run group "{{},{}}"
run group "{<a>,<a>,<a>,<a>}"
run group "{{<a>},{<a>},{<a>},{<a>}}"
run group "{{<!>},{<!>},{<!>},{<a>}}"
run garbage "<!!!>>"
run garbage "<{o\"i!a,<{i<a>"

let testInput = System.IO.File.ReadAllText "9.txt" |> fun x -> x.Trim ()

let parsedTestInput = run group testInput

let score (ast: Content) =
    let mutable scores = []
    let getGroups contents =
        match contents with
        | Group g -> Seq.filter (function | Group _ -> true | _ -> false) g
        | _       -> Seq.empty
        |> (fun g -> printfn "Groups: %i" (Seq.length g); g)
    let rec scoreByLevel (n: int) (children: Content seq) =
        printfn "Score %i" n
        printfn "Children length %i" (Seq.length children)
        scores <- n :: scores
        (Seq.sumBy
                (function
                    | Group g -> printfn"going down"; (scoreByLevel (n + 1) g)
                    | _ -> printfn "empty. %i" n; n + 1)
            children)

    scoreByLevel 1 (getGroups ast)
    List.sum scores

match parsedTestInput with
| Success (r, us, p) -> score r
| Failure (s, pe, us) -> failwith s