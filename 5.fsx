let testInput = [0; 3; 0; 1; -3]

let input =
    System.IO.File.ReadAllLines "5.txt"
    |> List.ofArray
    |> List.map int

let increment index =
    List.mapi
        ( fun mapI value ->
            match (mapI, value) with
            | (i,v) when (i = index) && (v >= 3) -> v - 1
            | (i,v) when i = index -> v + 1
            | (_,v) -> v )

let rec doIt index (depth: int) list =
    if depth % 100000 = 0 then
        Seq.filter (fun x -> x < -10) list
        |> Seq.length
        |> printfn "%i"
        //(printfn "%A" list)
    //System.Threading.Thread.Sleep (System.TimeSpan.FromMilliseconds 100.0)
    if (index >= List.length list) || index < 0
    then
        depth
    else
        let nextOffset = index + list.[index]
        let newList    = list |> increment index
        doIt nextOffset (depth + 1) newList

doIt 0 0 testInput

doIt 0 0 input


let doItInPlace (input: int[]) =
    let mutable index = 0
    let mutable count = 0
    while index < input.Length do
        let value = input.[index]
        let newValue = if value >= 3 then value - 1 else value + 1
        Array.set input index newValue
        index <- index + value
        count <- count + 1
    count