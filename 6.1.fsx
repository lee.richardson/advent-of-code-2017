let testInput = [0;2;7;0]
let input = [4;1;15;12;0;9;9;5;5;8;7;3;14;5;12;3]

let getElementToRedistribute list =
    list
    |> List.indexed
    |> List.maxBy snd
    |> fst

let emptyBank index list =
    let blocks = List.item index list

    let newList =
        list
        |> List.indexed
        |> List.map (fun (i, value) -> if i = index then 0 else value)

    (blocks, newList)

let incAt index list =
    list
    |> List.indexed
    |> List.map (fun (i, value) ->
        if i = (index % list.Length) then value + 1 else value)

let tryAddToSet value set =
    let newSet = Set.add value set
    if (Set.count newSet) = (Set.count set)
    then Result.Error set
    else Result.Ok newSet

let doIt list =
    let mutable count = 0
    let mutable isDone = false
    let mutable doItList = list
    let mutable iterations = Set.empty

    while not isDone do
        let index = getElementToRedistribute list

        doItList <- list
        |> emptyBank index
        |> (fun (blocks, list) ->
            List.fold
                (fun state i ->
                    let inced = incAt i state
                    tryAddToSet inced iterations
                    |> Result.mapError (fun oldSet -> isDone <- true; oldSet)
                    |> Result.map (fun newSet -> iterations <- newSet; count <- count + 1; newSet)
                    |> ignore
                    inced)
                list
                [1..blocks])
    count

doIt testInput
