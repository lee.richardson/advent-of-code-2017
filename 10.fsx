let testInput = "0, 1, 2, 3, 4"
open System

let (><) f x y = f y x

let parseInput (input: string) =
    input.Split([|','|], StringSplitOptions.RemoveEmptyEntries)
    |> Array.toList
    |> List.map
        (fun x -> x.Trim()
        >> int)

let wrappedWindowIndexes startingIndex windowLength listLength =
    [startingIndex .. (startingIndex + windowLength - 1)]
    |> List.map (fun x -> x % listLength)

let reverseSub index length (list: int list) =
    let reversedSub =
        wrappedWindowIndexes index length list.Length
        |> List.map (fun x -> List.item x list)
        |> List.rev
    reversedSub

    (*
    list.[0 .. index]
    @ list.[index .. (index + length)]
    @  *)

parseInput testInput

reverseSub 2 2 [1;2;3]
